import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  //property with a set method, the property name is same as selector for binding without []
  @Input() set appUnless(condition: boolean) {
    if (!condition) {
      this.vcRef.createEmbeddedView(this.templateRef);
    } else {
      this.vcRef.clear();
    }

  }

  //inject template that it will eventually sit on.
  //render on the ViewContainer
  constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) { }

}
