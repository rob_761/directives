import {Directive, ElementRef, OnInit} from "@angular/core";

@Directive({
  selector: '[appBasicHighlight]'
})

export class BasicHighlightDirective implements OnInit{

  //get access to the element the directive sits on
  constructor(private elementRef: ElementRef) {

  }

  ngOnInit() {
  this.elementRef.nativeElement.style.backgroundColor = 'green';
  }


}
