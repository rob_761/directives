import {Directive, ElementRef, HostBinding, HostListener, OnInit, Renderer2, Input} from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit{

  @Input() defaultColor: string = 'transparent';
  @Input('appBetterHighlight') highlightColor: string = 'blue'; //Directive name as alias for highlightColor


  //bind to property on the hosting element
  //backgroundColor is a sub property of style
  @HostBinding('style.backgroundColor') backgroundColor: string;
  //@HostBinding('style.backgroundColor') backgroundColor: string = this.defaultColor; //does not set properly

  constructor(private elRef: ElementRef, private rederer: Renderer2) { }

  ngOnInit() {
    this.backgroundColor = this.defaultColor; //ensures background color is set properly
  }

  //'mouseenter' is an event supported by DOM element this directive sits on
  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.backgroundColor = this.highlightColor;
   // this.rederer.setStyle( this.elRef.nativeElement,'background-color','blue', ) //need access to underlying element

  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.backgroundColor = this.defaultColor;
  }
}
